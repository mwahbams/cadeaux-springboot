package com.mwahba.cadeaux.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mwahba.cadeaux.entitiy.Note;

@Repository
public interface NoteRepository extends PagingAndSortingRepository<Note, Long> {

}
