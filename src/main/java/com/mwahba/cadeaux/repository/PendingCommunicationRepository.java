package com.mwahba.cadeaux.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mwahba.cadeaux.entitiy.PendingCommunication;

/**
 * 
 * @author mahmoudwahba
 * @date Mar 12, 2020 , 12:31:34 PM
 */

@Repository
public interface PendingCommunicationRepository extends JpaRepository<PendingCommunication, Long> {
	
	Optional<PendingCommunication> findByMobile(String mobile);

}
