package com.mwahba.cadeaux.exception;

public class ControllerException extends Exception {

	private static final long serialVersionUID = 1670704743829610721L;

	public ControllerException(String message, Throwable cause) {
		super(message, cause);
	}

	public ControllerException(String message) {
		super(message);
	}

	public ControllerException(Throwable cause) {
		super(cause);
	}

}
