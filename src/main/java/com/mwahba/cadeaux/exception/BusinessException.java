package com.mwahba.cadeaux.exception;

public class BusinessException extends Exception {

	private static final long serialVersionUID = 1670704743829610721L;

	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	public BusinessException(String message) {
		super(message);
	}

	public BusinessException(Throwable cause) {
		super(cause);
	}
	
}
