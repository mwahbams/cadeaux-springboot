package com.mwahba.cadeaux.exception;

public class SecException extends Exception{

	private static final long serialVersionUID = -2803786577532215162L;

	public SecException(String message, Throwable cause) {
		super(message, cause);
	}

	public SecException(String message) {
		super(message);
	}

	public SecException(Throwable cause) {
		super(cause);
	}

}
