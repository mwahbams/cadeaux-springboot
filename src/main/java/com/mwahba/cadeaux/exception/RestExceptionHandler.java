package com.mwahba.cadeaux.exception;

import java.sql.SQLException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.mwahba.cadeaux.entitiy.ErrorResponse;
import com.mwahba.cadeaux.util.MessageResolverUtils;
import com.mwahba.cadeaux.util.SessionUtils;



@ControllerAdvice
public class RestExceptionHandler {
	
	private static Logger LOGGER = LoggerFactory.getLogger(RestExceptionHandler.class);
	
	@Autowired
	private MessageResolverUtils messageResolverUtils;
	
	private static Locale bcLocale = new Locale("bc");
	
	//add exception
	@ExceptionHandler
	public ResponseEntity<ErrorResponse> handleException(Exception exc){
		LOGGER.error("RestExceptionHandler : ", exc);
		ErrorResponse  error = new ErrorResponse();
		if(exc instanceof SQLException)
		{
			error.setMessage(getLocalizedErrorMessage("database.error"));
			error.setBusinessCode(getLocalizedBCMessage(exc.getMessage()));
		}
		else {
			error.setMessage(getLocalizedErrorMessage(exc.getMessage()));
			error.setBusinessCode(getLocalizedBCMessage(exc.getMessage()));
		}

		error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setTimestamp(System.currentTimeMillis());
		
		
		return new ResponseEntity<>(error,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	
	private String getLocalizedErrorMessage(String errorCode) {
		
	return messageResolverUtils.resolveMessage(errorCode, SessionUtils.getCurrentLocale());
	}
	private String getLocalizedBCMessage(String errorCode) {
		return messageResolverUtils.resolveMessage(errorCode, bcLocale);
	}
	

}
