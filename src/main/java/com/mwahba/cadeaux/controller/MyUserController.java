package com.mwahba.cadeaux.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mwahba.cadeaux.entitiy.MyUser;
import com.mwahba.cadeaux.service.MyUserDetailsService;

@RestController
@RequestMapping(value = "/api/v1", produces = { MediaType.APPLICATION_JSON_VALUE })
public class MyUserController {

	@Autowired
	private MyUserDetailsService myUserDetailsService;

	@PostMapping("/user")
	ResponseEntity<MyUser> createNote(@RequestBody MyUser myUser) {
		System.out.println("myUser");
		return new ResponseEntity<MyUser>(myUserDetailsService.createUser(myUser),HttpStatus.CREATED);
	}


}
