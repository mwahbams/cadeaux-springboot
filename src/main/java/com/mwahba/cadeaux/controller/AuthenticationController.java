package com.mwahba.cadeaux.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mwahba.cadeaux.entitiy.AuthenticationRequest;
import com.mwahba.cadeaux.entitiy.AuthenticationResponse;
import com.mwahba.cadeaux.entitiy.Customer;
import com.mwahba.cadeaux.entitiy.MyUser;
import com.mwahba.cadeaux.exception.ControllerException;
import com.mwahba.cadeaux.service.CustomerDetailsService;
import com.mwahba.cadeaux.util.JwtUtil;

@RestController
public class AuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtUtil jwtTokenUtil;

	@Autowired
	private CustomerDetailsService customerDetailsService;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<AuthenticationResponse> createAuthenticationToken(
			@RequestBody AuthenticationRequest authenticationRequest) throws Exception {

		if (authenticationRequest == null || authenticationRequest.getMobile() == null
				|| authenticationRequest.getPassword() == null)
			throw new ControllerException("security.username.password.notExist");
		try {

			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					authenticationRequest.getMobile().trim(), authenticationRequest.getPassword().trim()));
		} catch (BadCredentialsException e) {
			throw new ControllerException("security.username.password.invalid");
		}

		final UserDetails userDetails = customerDetailsService.loadUserByUsername(authenticationRequest.getMobile().trim());

		final String token = jwtTokenUtil.generateToken(userDetails);
		Customer customer = customerDetailsService.findByMobile(authenticationRequest.getMobile().trim());
		customer.setPassWord(null);
		return ResponseEntity.ok(new AuthenticationResponse(token, customer));
	}

}
