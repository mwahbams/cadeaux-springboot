package com.mwahba.cadeaux.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mwahba.cadeaux.exception.ControllerException;

@RestController
public class HomeController {
	
	@GetMapping("/")
	public String home() throws Exception{
		throw new ControllerException("Teatss");
		
		//return "Welcome Every one";
	}
	
	@GetMapping("/user")
	public String user(){
		return "Welcome USer";
	}
	
	@GetMapping("/admin")
	public String admin(){
		return "Welcome Admin";
	}
	
	@GetMapping("/guest")
	public String guest(){
		return "Welcome Guest";
	}
	


}
