package com.mwahba.cadeaux.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mwahba.cadeaux.entitiy.AuthenticationResponse;
import com.mwahba.cadeaux.entitiy.Note;
import com.mwahba.cadeaux.service.NoteService;

@RestController
//@RequestMapping(value = "/api/v1", produces = { MediaType.APPLICATION_JSON_VALUE })
public class NoteController {

	@Autowired
	private NoteService noteService;

	@GetMapping(value = "/note")
	ResponseEntity<List<Note>> getAllNoted() {
		List<Note> notes = noteService.getAllNoted();
		
		return ResponseEntity.ok(notes);
	}
	
	@GetMapping(value = "/note/page")
	ResponseEntity<List<Note>> getAllNotesPage(@RequestParam int page ,@RequestParam int size) {
		List<Note> notes = noteService.getAllNotesPage(page, size);
		
		return ResponseEntity.ok(notes);
	}

	@GetMapping("/note/{id}")
	ResponseEntity<Note> getNoteById(@PathVariable Long id) {
		return ResponseEntity.ok(noteService.getNoteById(id));
	}

	@PostMapping("/note")
	ResponseEntity<Note> createNote(@RequestBody Note newNote) {
		return new ResponseEntity<Note>(noteService.createNote(newNote),HttpStatus.CREATED);
	}

	@PutMapping("/note/{id}")
	ResponseEntity<Note> updateNote(@RequestBody Note newNote, @PathVariable Long id) {

		return ResponseEntity.ok(noteService.createNote(newNote));
	}

	@DeleteMapping("/note/{id}")
	ResponseEntity<?> deleteNote(@PathVariable Long id) {
		return new ResponseEntity(HttpStatus.NO_CONTENT);
	}

}
