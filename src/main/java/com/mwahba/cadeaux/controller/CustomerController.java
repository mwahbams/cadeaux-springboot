package com.mwahba.cadeaux.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mwahba.cadeaux.entitiy.Customer;
import com.mwahba.cadeaux.entitiy.UserVerificationRequest;
import com.mwahba.cadeaux.service.CustomerService;
import com.mwahba.cadeaux.util.SessionUtils;


@RestController
//@RequestMapping(value = "/api/v1", produces = { MediaType.APPLICATION_JSON_VALUE })
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@GetMapping(value = "/customer")
	ResponseEntity<List<Customer>> getAllCustomer() {
		return ResponseEntity.ok(customerService.getAllCustomer());
	}

	@GetMapping("/customer/{id}")
	ResponseEntity<Customer> getCustomerById(@PathVariable Long id) {
		return ResponseEntity.ok(customerService.getCustomerById(id));
	}

	@PostMapping("/customer")
	ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {

		return new ResponseEntity<Customer>(customerService.createCustomer(customer),HttpStatus.CREATED);
	}
	
	
	@PostMapping("/customer/signup")
	ResponseEntity<Customer> signupCustomer(@RequestBody Customer customer) {
		
		return new ResponseEntity<Customer>(customerService.signupCustomer(customer),HttpStatus.CREATED);
	}

	@PutMapping("/customer/{id}")
	ResponseEntity<Customer> updateCustomer(@RequestBody Customer customer, @PathVariable Long id) {

		return ResponseEntity.ok(customerService.createCustomer(customer));
	}

	@DeleteMapping("/customer/{id}")
	ResponseEntity<?> deleteCustomer(@PathVariable Long id) {
		return new ResponseEntity(HttpStatus.NO_CONTENT);
	}
	
	
	@PostMapping("/customer/verification-code")
	ResponseEntity<?> generateVerification(UserVerificationRequest entity) throws Exception {
		
		//userVerificationHistoryService.generateLink(entity);
		//pendingCommunicationsTrnService.generateVerificationCode(entity);
		return new ResponseEntity(HttpStatus.NO_CONTENT);
	}

}
