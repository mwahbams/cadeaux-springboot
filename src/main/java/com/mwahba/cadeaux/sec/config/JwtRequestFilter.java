package com.mwahba.cadeaux.sec.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mwahba.cadeaux.entitiy.ErrorResponse;
import com.mwahba.cadeaux.service.CustomerDetailsService;
import com.mwahba.cadeaux.util.JwtUtil;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

	@Autowired
	private CustomerDetailsService userDetailsService;

	@Autowired
	private JwtUtil jwtUtil;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {

		final String authorizationHeader = request.getHeader("Authorization");

		String mobile = null;
		String jwt = null;
		try {
			if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
				jwt = authorizationHeader.substring(7);
				mobile = jwtUtil.extractUsername(jwt);
			}

			if (mobile != null && SecurityContextHolder.getContext().getAuthentication() == null) {

				UserDetails userDetails = this.userDetailsService.loadUserByUsername(mobile);

				if (jwtUtil.validateToken(jwt, userDetails)) {

					UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
							userDetails, null, userDetails.getAuthorities());
					usernamePasswordAuthenticationToken
							.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
					SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
				}
			}
			chain.doFilter(request, response);
		} catch (ExpiredJwtException e) {
			ErrorResponse  error = new ErrorResponse();
			
			error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			error.setMessage("Token Expired");
			error.setTimestamp(System.currentTimeMillis());
			error.setBusinessCode("BCSEC001");
			//error.setErrorLayer("Security");
			
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setContentType(MediaType.APPLICATION_JSON.toString());
			response.getWriter().write(convertObjectToJson(error));
		} catch (SignatureException e) {
			ErrorResponse  error = new ErrorResponse();
			
			error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			error.setMessage("Token Signature Invalid");
			error.setTimestamp(System.currentTimeMillis());
			error.setBusinessCode("BCSEC002");
			//error.setErrorLayer("Security");
			
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setContentType(MediaType.APPLICATION_JSON.toString());
			response.getWriter().write(convertObjectToJson(error));
		} catch(Exception e){
			ErrorResponse  error = new ErrorResponse();
			
			error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			error.setMessage(e.getMessage());
			error.setTimestamp(System.currentTimeMillis());
			error.setBusinessCode("BCSEC000");
			//error.setErrorLayer("Security");
			
			response.setContentType(MediaType.APPLICATION_JSON.toString());
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.getWriter().write(convertObjectToJson(error));
		}
		
	}
	
	
    private String convertObjectToJson(Object object) throws JsonProcessingException {
        if (object == null) {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }

}
