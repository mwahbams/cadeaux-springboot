/**
 * 
 */
package com.mwahba.cadeaux.util;

import java.util.Date;
import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;

import com.mwahba.cadeaux.entitiy.BaseEntity;

/**
 * 
 * @author mahmoudwahba
 * @date Mar 7, 2020 , 5:12:37 PM
 */
public final class SessionUtils {

	private SessionUtils() {

	}

//	public static UserInfo getCurrentUser() {
//		Object prinicipal = SecurityContextHolder.getContext() != null
//				&& SecurityContextHolder.getContext().getAuthentication() != null
//						? SecurityContextHolder.getContext().getAuthentication().getPrincipal()
//						: null;
//		return prinicipal != null && prinicipal instanceof UserDetails ? ((DefaultUserDetails) prinicipal).getUserInfo()
//				: null;
//	}
	
	public static BaseEntity entityCreatedInfo(BaseEntity baseentity) {
		baseentity.setTenantId(1);
		baseentity.setCreatedDate(new Date());
		baseentity.setCreatedFromChannelId("001");
		baseentity.setStatusCode(1);
		baseentity.setCreatedByUserId(1L);
		
		return baseentity;
	}
	
	public static BaseEntity entityUpdatedInfo(BaseEntity baseentity) {
		//baseentity.setTenantId(1);
		baseentity.setModifiedDate(new Date());
		baseentity.setModifiedFromChannelId("001");
		//baseentity.setStatusCode(1);
		baseentity.setModifiedByUserId(1L);
		
		return baseentity;
	}

	public static Locale getCurrentLocale() {
		return LocaleContextHolder.getLocale();
	}
}
