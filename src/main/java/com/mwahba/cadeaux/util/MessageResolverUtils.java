/**
 * 
 */
package com.mwahba.cadeaux.util;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Component;

/**
 * 
 * @author mahmoudwahba
 * @date Mar 7, 2020 , 6:15:22 PM
 */
@Component
public class MessageResolverUtils {
	//private static final Logger LOGGER = LoggerFactory.getLogger(MessageResolverUtils.class);

	@Autowired
	@Qualifier("messagesSource")
	private MessageSource messageSource;
	
	public String resolveMessage(String code, Locale locale) {
		return resolveMessage(code, null, locale);
	}

	public String resolveMessage(String code, Object[] args, Locale locale) {
		String message = null;
		try {
			message = messageSource.getMessage(code, args, locale);
		} catch (NoSuchMessageException e) {
			message = "##" + code + "##";
			//LOGGER.error(message + " Not Found", e);
		}
		catch (RuntimeException e) {
			message = "##" + code + "##";
			//LOGGER.error("An Error has occurred", e);
		} 
		return message;
	}
}
