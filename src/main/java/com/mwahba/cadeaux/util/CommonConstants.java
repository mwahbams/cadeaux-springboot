package com.mwahba.cadeaux.util;

public class CommonConstants {
	
	// 
	public static final  String FORGET_PASSWORD ="FORGET_PASSWORD";
	public static final  String  REST_PASSWORD = "REST_PASSWORD";
	public static final  String  NEW_PASSWORD = "NEW_PASSWORD";
	
	
	public static final String  FUNCTION_SIGNUP = "signup";
	public static final String  FUNCTION_FORGET_PASSWORD = "forget-password";
	public static final String  FUNCTION_RECALAIM = "reclaim";
	public static final String  FUNCTION_RESET_PASSWORD = "reset-password";
	
	public static final  String FULL_DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";
	public static final  String DATE_FORMAT = "yyyy-MM-dd";
	
	// LINK_STATUS_LKP
	public static final  Long PENDING = 1L;
	public static final  Long SENT = 2L;
	public static final  Long FAILED = 3L;
	public static final  Long OPENED = 4L;
	public static final  Long EXPIRED = 5L;
	public static final  String URL = "http://172.16.10.231:7070/ecommerce-admin-usermanagement-web/change-password";


}
