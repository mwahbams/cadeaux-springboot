package com.mwahba.cadeaux.entitiy;

import java.io.Serializable;

public class AuthenticationResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8507231566962780978L;
	private Customer customer;
	private final String token;


	public AuthenticationResponse(String token) {
		this.token = token;
	}

	public AuthenticationResponse(String token, Customer customer) {
		super();
		this.token = token;
		this.customer = customer;
	}



	public String getToken() {
		return token;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}
