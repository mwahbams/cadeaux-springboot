package com.mwahba.cadeaux.entitiy;

import java.io.Serializable;

public class AuthenticationRequest implements Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = -4712177544547962227L;
	private String username;
	private String mobile;
    public AuthenticationRequest(String mobile, String password) {
		super();
		this.mobile = mobile;
		this.password = password;
	}
//    public AuthenticationRequest(String username, String password) {
//        this.setUsername(username);
//        this.setPassword(password);
//    }

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //need default constructor for JSON Parsing
    public AuthenticationRequest()
    {

    }


}
