package com.mwahba.cadeaux.entitiy;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mwahba.cadeaux.util.CommonConstants;

@Entity
@Table(name = "NOTE")
public class Note {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "title")
	private String title;
	@Column(name = "content")
	private String content;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = CommonConstants.FULL_DATE_FORMAT)
	@Column(name = "creation_date")
	private Date creationDate = new Date();
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = CommonConstants.FULL_DATE_FORMAT)
	@Column(name = "modified_date")
	private Date modifiedDate;



	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
