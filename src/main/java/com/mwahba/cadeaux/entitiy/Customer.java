package com.mwahba.cadeaux.entitiy;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mwahba.cadeaux.util.CommonConstants;

/**
 * 
 * @author mahmoudwahba
 * @date Mar 12, 2020 , 12:29:40 PM
 */

@Entity
@Table(name = "customers_trn")
public class Customer extends BaseEntity {

	@Column(name = "FULL_NAME")
	private String fullName;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = CommonConstants.FULL_DATE_FORMAT)
	@Column(name = "BIRTH_DATE")
	private Date birthDate;

	@Column(name = "MOBILE")
	private String mobile;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "USER_NAME")
	private String userName;

	@Column(name = "PASS_WORD")
	private String passWord;

	@Column(name = "IMAGE_PATH")
	private String imagePath;

	@Column(name = "ACCOUNT_PUBLIC")
	private Integer accountPublic;

	@Column(name = "GENDER")
	private String gender;

	@Column(name = "CUSTOMER_APP_DEVICE_TOKEN")
	private String customerAppDeviceToken;

	@Column(name = "CUSTOMER_LAST_APP_DEVICE_TOKEN_DATE")
	private String customerAccessToken;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = CommonConstants.FULL_DATE_FORMAT)
	@Column(name = "CUSTOMER_ACCESS_TOKEN")
	private Date customerLastAccessTokenDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = CommonConstants.FULL_DATE_FORMAT)
	@Column(name = "CUSTOMER_LAST_ACCESS_TOKEN_DATE")
	private Date customerLastAppDeviceTokenDate;

	@Column(name = "CUSTOMER_STATUS_TYPE_ID")
	private Integer customerStatusTypeId;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = CommonConstants.FULL_DATE_FORMAT)
	@Column(name = "CUSTOMER_LAST_STATUS_DATE")
	private Date customerLastStatusDate;

	@Column(name = "LANGUAGE_ID")
	private Integer languageId;

	@Column(name = "NATIONALITY_COUNTRY_ID")
	private Integer nationalityCountryId;

	@Column(name = "RESIDENCE_COUNTRY_ID")
	private Integer residenceCountryId;

	@Column(name = "CUSTOMER_TITLE_ID")
	private Integer customerTitleId;
	
	@Column(name = "ACTIVE")
	private boolean active;
	
	@Column(name = "ROLES")
	private String roles;
	
	@Transient
	private String verficationCode;
	@Transient
	private String communicationFunction;


	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public Integer getAccountPublic() {
		return accountPublic;
	}

	public void setAccountPublic(Integer accountPublic) {
		this.accountPublic = accountPublic;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCustomerAppDeviceToken() {
		return customerAppDeviceToken;
	}

	public void setCustomerAppDeviceToken(String customerAppDeviceToken) {
		this.customerAppDeviceToken = customerAppDeviceToken;
	}

	public String getCustomerAccessToken() {
		return customerAccessToken;
	}

	public void setCustomerAccessToken(String customerAccessToken) {
		this.customerAccessToken = customerAccessToken;
	}

	public Date getCustomerLastAccessTokenDate() {
		return customerLastAccessTokenDate;
	}

	public void setCustomerLastAccessTokenDate(Date customerLastAccessTokenDate) {
		this.customerLastAccessTokenDate = customerLastAccessTokenDate;
	}

	public Date getCustomerLastAppDeviceTokenDate() {
		return customerLastAppDeviceTokenDate;
	}

	public void setCustomerLastAppDeviceTokenDate(Date customerLastAppDeviceTokenDate) {
		this.customerLastAppDeviceTokenDate = customerLastAppDeviceTokenDate;
	}

	public Integer getCustomerStatusTypeId() {
		return customerStatusTypeId;
	}

	public void setCustomerStatusTypeId(Integer customerStatusTypeId) {
		this.customerStatusTypeId = customerStatusTypeId;
	}

	public Date getCustomerLastStatusDate() {
		return customerLastStatusDate;
	}

	public void setCustomerLastStatusDate(Date customerLastStatusDate) {
		this.customerLastStatusDate = customerLastStatusDate;
	}

	public Integer getNationalityCountryId() {
		return nationalityCountryId;
	}

	public void setNationalityCountryId(Integer nationalityCountryId) {
		this.nationalityCountryId = nationalityCountryId;
	}

	public Integer getResidenceCountryId() {
		return residenceCountryId;
	}

	public void setResidenceCountryId(Integer residenceCountryId) {
		this.residenceCountryId = residenceCountryId;
	}

	public Integer getCustomerTitleId() {
		return customerTitleId;
	}

	public void setCustomerTitleId(Integer customerTitleId) {
		this.customerTitleId = customerTitleId;
	}

	@Override
	public String toString() {
		return "Customer [fullName=" + fullName + ", birthDate=" + birthDate + ", mobile=" + mobile + ", email=" + email
				+ ", userName=" + userName + ", passWord=" + passWord + ", imagePath=" + imagePath + ", accountPublic="
				+ accountPublic + ", gender=" + gender + ", customerAppDeviceToken=" + customerAppDeviceToken
				+ ", customerAccessToken=" + customerAccessToken + ", customerLastAccessTokenDate="
				+ customerLastAccessTokenDate + ", customerLastAppDeviceTokenDate=" + customerLastAppDeviceTokenDate
				+ ", customerStatusTypeId=" + customerStatusTypeId + ", customerLastStatusDate="
				+ customerLastStatusDate + ", languageId=" + languageId + ", nationalityCountryId="
				+ nationalityCountryId + ", residenceCountryId=" + residenceCountryId + ", customerTitleId="
				+ customerTitleId + "]";
	}

	public Integer getLanguageId() {
		return languageId;
	}

	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public String getVerficationCode() {
		return verficationCode;
	}

	public void setVerficationCode(String verficationCode) {
		this.verficationCode = verficationCode;
	}

	public String getCommunicationFunction() {
		return communicationFunction;
	}

	public void setCommunicationFunction(String communicationFunction) {
		this.communicationFunction = communicationFunction;
	}

}
