/**
 * 
 */
package com.mwahba.cadeaux.entitiy;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mwahba.cadeaux.util.CommonConstants;

/**
 * 
 * @author mahmoudwahba
 * @date Mar 12, 2020 , 12:25:27 PM
 */

@MappedSuperclass
public abstract class BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "status_code")
	private Integer statusCode ;

	@Column(name = "comments")
	private String comments;

	@Column(name = "tenant_id")
	private Integer tenantId;

	@Column(name = "row_deleted")
	private Integer rowDeleted;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = CommonConstants.FULL_DATE_FORMAT)
	@Column(name = "created_date")
	private Date createdDate ;

	@Column(name = "created_by_user_id")
	private Long createdByUserId;

	@Column(name = "created_from_channel_id")
	private String createdFromChannelId ;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = CommonConstants.FULL_DATE_FORMAT)
	@Column(name = "modified_date")
	private Date modifiedDate;

	@Column(name = "modified_by_user_id")
	private Long modifiedByUserId;

	@Column(name = "modified_from_channel_id")
	private String modifiedFromChannelId;

	@Column(name = "end_user_ip")
	private String endUserIp;

	@Column(name = "server_ip")
	private String serverIp;

	@Column(name = "operating_system")
	private String operatingSystem;

	@Column(name = "operating_system_version")
	private String operatingSystemVersion;

	@Column(name = "application_name")
	private String applicationName;

	@Column(name = "application_version")
	private String applicationVersion;

	@Column(name = "device_identifier")
	private String deviceIdentifier;

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getTenantId() {
		return tenantId;
	}

	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}

	public Integer getRowDeleted() {
		return rowDeleted;
	}

	public void setRowDeleted(Integer rowDeleted) {
		this.rowDeleted = rowDeleted;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedByUserId() {
		return createdByUserId;
	}

	public void setCreatedByUserId(Long createdByUserId) {
		this.createdByUserId = createdByUserId;
	}

	public String getCreatedFromChannelId() {
		return createdFromChannelId;
	}

	public void setCreatedFromChannelId(String createdFromChannelId) {
		this.createdFromChannelId = createdFromChannelId;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedFromChannelId() {
		return modifiedFromChannelId;
	}

	public void setModifiedFromChannelId(String modifiedFromChannelId) {
		this.modifiedFromChannelId = modifiedFromChannelId;
	}

	public String getEndUserIp() {
		return endUserIp;
	}

	public void setEndUserIp(String endUserIp) {
		this.endUserIp = endUserIp;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public String getOperatingSystem() {
		return operatingSystem;
	}

	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}

	public String getOperatingSystemVersion() {
		return operatingSystemVersion;
	}

	public void setOperatingSystemVersion(String operatingSystemVersion) {
		this.operatingSystemVersion = operatingSystemVersion;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}

	public String getDeviceIdentifier() {
		return deviceIdentifier;
	}

	public void setDeviceIdentifier(String deviceIdentifier) {
		this.deviceIdentifier = deviceIdentifier;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "BaseEntity [id=" + id + ", statusCode=" + statusCode + ", comments=" + comments + ", tenantId="
				+ tenantId + ", rowDeleted=" + rowDeleted + ", createdDate=" + createdDate + ", createdByUserId="
				+ createdByUserId + ", createdFromChannelId=" + createdFromChannelId + ", modifiedDate=" + modifiedDate
				+ ", modifiedByUserId=" + modifiedByUserId + ", modifiedFromChannelId=" + modifiedFromChannelId
				+ ", endUserIp=" + endUserIp + ", serverIp=" + serverIp + ", operatingSystem=" + operatingSystem
				+ ", operatingSystemVersion=" + operatingSystemVersion + ", applicationName=" + applicationName
				+ ", applicationVersion=" + applicationVersion + ", deviceIdentifier=" + deviceIdentifier + "]";
	}

	public Long getModifiedByUserId() {
		return modifiedByUserId;
	}

	public void setModifiedByUserId(Long modifiedByUserId) {
		this.modifiedByUserId = modifiedByUserId;
	}

}
