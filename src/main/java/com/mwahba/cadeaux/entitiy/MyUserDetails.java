package com.mwahba.cadeaux.entitiy;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class MyUserDetails implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3144992202053953777L;

	private String userName;
	private String mobile;
	private String password;
	private boolean active;
	private List<GrantedAuthority> authorities;

	public MyUserDetails(Customer customer) {
		this.userName = customer.getUserName();
		this.mobile = customer.getMobile();
		this.active = customer.isActive();
		this.password = customer.getPassWord();
		this.authorities = Arrays.stream(customer.getRoles().split(",")).map(SimpleGrantedAuthority::new)
				.collect(Collectors.toList());
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {

		return password;
	}

	@Override
	public String getUsername() {
		return mobile;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return active;
	}

	public String getMobile() {
		return mobile;
	}

}
