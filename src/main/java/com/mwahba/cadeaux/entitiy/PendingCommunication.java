package com.mwahba.cadeaux.entitiy;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 
 * @author mahmoudwahba
 * @date Mar 12, 2020 , 2:48:05 PM
 */

@Entity
@Table(name = "pending_communications_trn")
public class PendingCommunication extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8316941619795679403L;

	@Column(name = "MESSAGE_ID")
	private Integer messageId ;
	
	@Column(name = "COMMUNICATION_DATE")
	private Date  communicationDate ;
	
	@Column(name = "MOBILE_NUMBER")
	private String mobile ;
	
	@Column(name = "EMAIL")
	private String email ;
	
	@Column(name = "DEVICE_TOKEN")
	private String deviceToken ;
	
	@Column(name = "COMMUNICATION_FUNCTION_ID")
	private Integer communicationFunctionId ;
	
	@Column(name = "COMM_CHANNEL_ID")
	private String commChannelId ;
	
	@Column(name = "VERIFICATION_TYPE_ID")
	private Integer verificationTypeId;
	
	@Column(name = "VERFICATION_CODE")
	private String verficationCode ;
	
	@Column(name = "EXPIRY_IN_MINUTES")
	private Integer expiryInMinutes ;
	
	@Column(name = "EXPIRY_DATE")
	private Date expiryDate ;
	
	@Column(name = "FUNCTION_KEY")
	private String functionKey ;
	
	@Column(name = "COMM_LENGTH")
	private Integer commLength ;
	
	@Column(name = "MAX_DAILY_ATTEMPTS")
	private Integer maxDailyAttempts ;
	
	@Column(name = "COMMUNICATION_GATEWAY_ID")
	private Integer communicationGatewayId ;
	
	@Column(name = "COMMUNICATION_TYPE_ID")
	private Integer communicationTypeId ;
	
	@Column(name = "COMMUNICATION_SETUP_ID")
	private Integer communicationSetupId ;
	
	@Column(name = "CUSTOMER_ID")
	private Integer customerId ;
	
	@Column(name = "COMMUNICATION_STATUS_TYPE_ID")
	private Integer communicationStatusTypeId ;
	
	@Column(name = "COMMUNICATION_CONTENT")
	private String communicationContent ;
	
	@Column(name = "LANGUAGE_ID")
	private Integer languageId ;
	
	@Column(name = "SUB_OPERATION_ID")
	private Integer subOperationId ;
	
	@Column(name = "BULK_COMM_CONTENT_ID")
	private Integer bulkCommContentId;
	
	
	public Integer getMessageId() {
		return messageId;
	}
	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}
	public Date getCommunicationDate() {
		return communicationDate;
	}
	public void setCommunicationDate(Date communicationDate) {
		this.communicationDate = communicationDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	public Integer getCommunicationFunctionId() {
		return communicationFunctionId;
	}
	public void setCommunicationFunctionId(Integer communicationFunctionId) {
		this.communicationFunctionId = communicationFunctionId;
	}
	public String getCommChannelId() {
		return commChannelId;
	}
	public void setCommChannelId(String commChannelId) {
		this.commChannelId = commChannelId;
	}
	public Integer getVerificationTypeId() {
		return verificationTypeId;
	}
	public void setVerificationTypeId(Integer verificationTypeId) {
		this.verificationTypeId = verificationTypeId;
	}
	public String getVerficationCode() {
		return verficationCode;
	}
	public void setVerficationCode(String verficationCode) {
		this.verficationCode = verficationCode;
	}
	public Integer getExpiryInMinutes() {
		return expiryInMinutes;
	}
	public void setExpiryInMinutes(Integer expiryInMinutes) {
		this.expiryInMinutes = expiryInMinutes;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getFunctionKey() {
		return functionKey;
	}
	public void setFunctionKey(String functionKey) {
		this.functionKey = functionKey;
	}
	public Integer getCommLength() {
		return commLength;
	}
	public void setCommLength(Integer commLength) {
		this.commLength = commLength;
	}
	public Integer getMaxDailyAttempts() {
		return maxDailyAttempts;
	}
	public void setMaxDailyAttempts(Integer maxDailyAttempts) {
		this.maxDailyAttempts = maxDailyAttempts;
	}
	public Integer getCommunicationGatewayId() {
		return communicationGatewayId;
	}
	public void setCommunicationGatewayId(Integer communicationGatewayId) {
		this.communicationGatewayId = communicationGatewayId;
	}
	public Integer getCommunicationTypeId() {
		return communicationTypeId;
	}
	public void setCommunicationTypeId(Integer communicationTypeId) {
		this.communicationTypeId = communicationTypeId;
	}
	public Integer getCommunicationSetupId() {
		return communicationSetupId;
	}
	public void setCommunicationSetupId(Integer communicationSetupId) {
		this.communicationSetupId = communicationSetupId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Integer getCommunicationStatusTypeId() {
		return communicationStatusTypeId;
	}
	public void setCommunicationStatusTypeId(Integer communicationStatusTypeId) {
		this.communicationStatusTypeId = communicationStatusTypeId;
	}
	public String getCommunicationContent() {
		return communicationContent;
	}
	public void setCommunicationContent(String communicationContent) {
		this.communicationContent = communicationContent;
	}
	public Integer getLanguageId() {
		return languageId;
	}
	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}
	public Integer getSubOperationId() {
		return subOperationId;
	}
	public void setSubOperationId(Integer subOperationId) {
		this.subOperationId = subOperationId;
	}
	public Integer getBulkCommContentId() {
		return bulkCommContentId;
	}
	public void setBulkCommContentId(Integer bulkCommContentId) {
		this.bulkCommContentId = bulkCommContentId;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
}
