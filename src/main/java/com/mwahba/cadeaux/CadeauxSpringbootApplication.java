package com.mwahba.cadeaux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages="com.mwahba.cadeaux.repository")
public class CadeauxSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadeauxSpringbootApplication.class, args);
	}

}
