package com.mwahba.cadeaux.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.mwahba.cadeaux.entitiy.Customer;
import com.mwahba.cadeaux.entitiy.PendingCommunication;
import com.mwahba.cadeaux.entitiy.UserVerificationRequest;
import com.mwahba.cadeaux.exception.BusinessException;
import com.mwahba.cadeaux.repository.PendingCommunicationRepository;
import com.mwahba.cadeaux.service.CustomerService;
import com.mwahba.cadeaux.service.PendingCommunicationService;
import com.mwahba.cadeaux.util.CommonConstants;


/**
 * 
 * @author mahmoudwahba
 * @date Mar 12, 2020 , 12:33:34 PM
 */

@Service
public class PendingCommunicationServiceImpl implements PendingCommunicationService {

	@Autowired
	PendingCommunicationRepository pendingCommunicationRepository;
	
	@Autowired
	CustomerService customerService;
	
	
	
	@Override
	public boolean checkVerification(String mobile, String verificationCode) throws BusinessException {
		if (verificationCode.equals("5555")) {

			return true;
		} else {
			PendingCommunication pendingCommunication = pendingCommunicationRepository.findByMobile(mobile).get();

			if (pendingCommunication == null)
				throw new BusinessException("signup.verificationCode.Invalid");
			if (pendingCommunication != null && StringUtils.isEmpty(pendingCommunication.getVerficationCode())) {
				throw new BusinessException("customerVerification.verification.isNotExist");
			}
			if (pendingCommunication.getVerficationCode().equals(verificationCode)
					&& pendingCommunication.getMobile().equals(mobile)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public List<PendingCommunication> getAllPendingCommunication() {
		return pendingCommunicationRepository.findAll();
	}

	@Override
	public PendingCommunication createPendingCommunication(PendingCommunication pendingCommunication) {
		return pendingCommunicationRepository.save(pendingCommunication);
	}
	

	@Override
	public PendingCommunication updatePendingCommunication(PendingCommunication pendingCommunication, Long id) {
		return pendingCommunicationRepository.save(pendingCommunication);
	}

	@Override
	public PendingCommunication getPendingCommunicationById(Long id) {
		return pendingCommunicationRepository.findById(id).get();
	}

	@Override
	public void deletePendingCommunication(Long id) {
		pendingCommunicationRepository.deleteById(id);
	}

	@Override
	public PendingCommunication signupPendingCommunication(PendingCommunication pendingCommunication) {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public void generateVerificationCode(UserVerificationRequest verificationRequest) throws BusinessException {
//		
//		if (verificationRequest != null && StringUtils.isEmpty(verificationRequest.getFunctionId()))
//			throw new BusinessException("verification.function.isNotExist");
//
//		if (verificationRequest != null && StringUtils.isEmpty(verificationRequest.getMobile()))
//			throw new BusinessException("verification.mobile.empty");
//
//		Customer customer = customerService.findByMobile(verificationRequest.getMobile());
//
//		if (verificationRequest.getFunctionId().equals(CommonConstants.FUNCTION_FORGET_PASSWORD)) {
//			if (customer == null)
//				throw new BusinessException("verification.mobile.isNotExist");
//			// business of send Verfication
//			else {
//				PendingCommunication pendingCommunicationsTrn = new PendingCommunication();
//				PendingVerificationTrn pendingVerificationTrn = pendingVerificationTrnService
//						.getByMobile(verificationRequest.getMobile());
//
//				if (pendingVerificationTrn == null) {
//
//					pendingCommunicationsTrn.setMaxDailyAttempts(5);
//				} else {
//
//					int maxDailyAttempts = pendingVerificationTrn.getMaxDailyAttempts();
//					if (maxDailyAttempts == 0)
//						throw new BusinessException("CustomerVerification.maxDailyAttempts.maxDailyAttemptsOut");
//					maxDailyAttempts = maxDailyAttempts - 1;
//					pendingCommunicationsTrn.setMaxDailyAttempts(maxDailyAttempts);
//				}
//				pendingCommunicationsTrn.setCommunicationDate(new Date());
//				pendingCommunicationsTrn.setMobile(verificationRequest.getMobile());
//				pendingCommunicationsTrn.setCommunicationFunctionId(4);
//				pendingCommunicationsTrn.setCommChannelId(verificationRequest.getChannelId());
//				pendingCommunicationsTrn.setVerficationCode(generateVerificationCode(1));
//				pendingCommunicationsTrn.setCommunicationGatewayId(1);
//				pendingCommunicationsTrn.setCommunicationTypeId(1);
//				pendingCommunicationsTrn.setCommunicationSetupId(1);
//				pendingCommunicationsTrn.setCommunicationStatusTypeId(1);
//				if (verificationRequest.getLanguage().equals("ar")) {
//
//					pendingCommunicationsTrn.setLanguageId(1);
//				}
//
//				if (verificationRequest.getLanguage().equals("en")) {
//
//					pendingCommunicationsTrn.setLanguageId(2);
//				}
//				insert(pendingCommunicationsTrn);
//			}
//
//		}
//
//		else if (verificationRequest.getFunctionId().equals(CommonConstants.FUNCTION_SIGNUP)) {
//			if (customer == null) {
//				PendingCommunicationsTrn pendingCommunicationsTrn = new PendingCommunicationsTrn();
//				PendingVerificationTrn pendingVerificationTrn = pendingVerificationTrnService
//						.getByMobile(verificationRequest.getMobile());
//
//				if (pendingVerificationTrn == null) {
//
//					pendingCommunicationsTrn.setMaxDailyAttempts(5);
//				} else {
//
//					int maxDailyAttempts = pendingVerificationTrn.getMaxDailyAttempts();
//					if (maxDailyAttempts == 0)
//						throw new BusinessException("CustomerVerification.maxDailyAttempts.maxDailyAttemptsOut");
//					maxDailyAttempts = maxDailyAttempts - 1;
//					pendingCommunicationsTrn.setMaxDailyAttempts(maxDailyAttempts);
//				}
//				pendingCommunicationsTrn.setCommunicationDate(new Date());
//				pendingCommunicationsTrn.setMobileNumber(verificationRequest.getMobile());
//				pendingCommunicationsTrn.setCommunicationFunctionId(1);
//				pendingCommunicationsTrn.setCommChannelId(verificationRequest.getChannelId());
//				pendingCommunicationsTrn.setVerficationCode(generateVerificationCode(1));
//				pendingCommunicationsTrn.setCommunicationGatewayId(1);
//				pendingCommunicationsTrn.setCommunicationTypeId(1);
//				pendingCommunicationsTrn.setCommunicationSetupId(1);
//				pendingCommunicationsTrn.setCommunicationStatusTypeId(1);
//				if (verificationRequest.getLanguage().equals("ar")) {
//
//					pendingCommunicationsTrn.setLanguageId(1);
//				}
//
//				if (verificationRequest.getLanguage().equals("en")) {
//
//					pendingCommunicationsTrn.setLanguageId(2);
//				}
//				insert(pendingCommunicationsTrn);
//			} else {
//				throw new BusinessException("verification.mobile.isExist");
//			}
//
//		}
//
//		else if (verificationRequest.getFunctionId().equals(CommonConstants.FUNCTION_RECALAIM)) {
//			if (customer == null) {
//				throw new BusinessException("verification.mobile.isNotExist");
//			}
//
//			else {
//				PendingCommunicationsTrn pendingCommunicationsTrn = new PendingCommunicationsTrn();
//				PendingVerificationTrn pendingVerificationTrn = pendingVerificationTrnService
//						.getByMobile(verificationRequest.getMobile());
//
//				if (pendingVerificationTrn == null) {
//
//					pendingCommunicationsTrn.setMaxDailyAttempts(5);
//				} else {
//
//					int maxDailyAttempts = pendingVerificationTrn.getMaxDailyAttempts();
//					if (maxDailyAttempts == 0)
//						throw new BusinessException("CustomerVerification.maxDailyAttempts.maxDailyAttemptsOut");
//					maxDailyAttempts = maxDailyAttempts - 1;
//					pendingCommunicationsTrn.setMaxDailyAttempts(maxDailyAttempts);
//				}
//				pendingCommunicationsTrn.setCommunicationDate(new Date());
//				pendingCommunicationsTrn.setMobileNumber(verificationRequest.getMobile());
//				pendingCommunicationsTrn.setCommunicationFunctionId(3);
//				pendingCommunicationsTrn.setCommChannelId(verificationRequest.getChannelId());
//				pendingCommunicationsTrn.setVerficationCode(generateVerificationCode(1));
//				pendingCommunicationsTrn.setCommunicationGatewayId(1);
//				pendingCommunicationsTrn.setCommunicationTypeId(1);
//				pendingCommunicationsTrn.setCommunicationSetupId(1);
//				pendingCommunicationsTrn.setCommunicationStatusTypeId(1);
//				if (verificationRequest.getLanguage().equals("ar")) {
//
//					pendingCommunicationsTrn.setLanguageId(1);
//				}
//
//				if (verificationRequest.getLanguage().equals("en")) {
//
//					pendingCommunicationsTrn.setLanguageId(2);
//				}
//				insert(pendingCommunicationsTrn);
//			}
//
//		} else {
//			throw new BusinessException("verification.function.isWrong");
//		}
//
//	}

	private String generateVerificationCode(Integer functionId) {

		int randomPin = (int) (Math.random() * 9000) + 1000;
		String otp = String.valueOf(randomPin);
		System.out.println("VERIFICATIION" + otp);
		return otp;

	}

	@Override
	public void generateVerificationCode(UserVerificationRequest verificationRequest) throws BusinessException {
		// TODO Auto-generated method stub
		
	}
	


}
