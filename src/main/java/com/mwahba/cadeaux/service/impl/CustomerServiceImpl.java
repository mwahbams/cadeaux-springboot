package com.mwahba.cadeaux.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.mwahba.cadeaux.entitiy.Customer;
import com.mwahba.cadeaux.exception.BusinessException;
import com.mwahba.cadeaux.repository.CustomerRepository;
import com.mwahba.cadeaux.service.CustomerService;
import com.mwahba.cadeaux.service.PendingCommunicationService;
import com.mwahba.cadeaux.util.CommonConstants;
import com.mwahba.cadeaux.util.SessionUtils;


/**
 * 
 * @author mahmoudwahba
 * @date Mar 12, 2020 , 12:33:34 PM
 */

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	PendingCommunicationService pendingCommunicationService;
	
	@Autowired
	PasswordEncoder passwordEncoder ;

	@Override
	public List<Customer> getAllCustomer() {
		return customerRepository.findAll();
	}

	@Override
	public Customer createCustomer(Customer customer) {
		Customer customerCreated = (Customer) SessionUtils.entityCreatedInfo(customer);
		customerCreated.setActive(true);
		customerCreated.setRoles("USER_ROLE");
		customer.setPassWord(passwordEncoder.encode(customer.getPassWord().trim()));
		return customerRepository.save(customer);
	}
	
	@Override
	public Customer signupCustomer(Customer customer) {
		
		
		Customer customerCreated = (Customer) SessionUtils.entityCreatedInfo(customer);
		customerCreated.setActive(true);
		customerCreated.setRoles("USER_ROLE");
		customer.setPassWord(passwordEncoder.encode(customer.getPassWord().trim()));
		return customerRepository.save(customer);
	}
	
	
	public Customer verifyRegisterCustomer(Customer customerRegisteration) throws BusinessException {
		if (StringUtils.isEmpty(customerRegisteration.getMobile()))
			throw new BusinessException("registeration.Mobile.Empty");
		if (StringUtils.isEmpty(customerRegisteration.getEmail()))
			throw new BusinessException("registeration.Email.Empty");
		if (customerRegisteration.getCommunicationFunction().equals(CommonConstants.FUNCTION_SIGNUP)) {

		Customer customer = customerRepository.findByMobile(customerRegisteration.getMobile()).get();

			if (customer != null)
				throw new BusinessException("signup.mobile.isExist");

			boolean check = pendingCommunicationService.checkVerification(customerRegisteration.getMobile(),
					customerRegisteration.getVerficationCode());

			if (check == true) {
				
			
				//customerRegisteration.setCommunicationFunctionId(1);
				Customer customerCreated = (Customer) SessionUtils.entityCreatedInfo(customerRegisteration);
				customerCreated.setActive(true);
				customerCreated.setRoles("USER_ROLE");
				customer.setPassWord(passwordEncoder.encode(customer.getPassWord().trim()));
				return customerRepository.save(customer);
			} else {
				throw new BusinessException("signup.verificationCode.Invalid");
			}
		} else if (customerRegisteration.getCommunicationFunction()
				.equals(CommonConstants.FUNCTION_RECALAIM)) {

			//customerRegisteration.getVerification().setCommunicationFunctionId(3);
			Customer customer = customerRepository.findByMobile(customerRegisteration.getMobile()).get();
		
			if (customer == null)
				throw new BusinessException("reclaim.mobile.isNotExist");
			if (customer.getEmail().equals(customerRegisteration.getEmail()))
				throw new BusinessException("reclaim.Email.Mobile.Exist");
			
			boolean check = pendingCommunicationService.checkVerification(customerRegisteration.getMobile(),
					customerRegisteration.getVerficationCode());

			if (check == true) {
				customer.setStatusCode(0);

				Customer customerModified = (Customer) SessionUtils.entityUpdatedInfo(customer);

				customerRepository.save(customerModified);
				
				
				Customer customerCreated = (Customer) SessionUtils.entityCreatedInfo(customerRegisteration);
				customerCreated.setActive(true);
				customerCreated.setRoles("USER_ROLE");
				customer.setPassWord(passwordEncoder.encode(customer.getPassWord().trim()));
				return customerRepository.save(customerCreated);
			} else
				throw new BusinessException("signup.verificationCode.Invalid");
		}

		else {
			throw new BusinessException("signup.communicationFunction.Invalid");
		}
	}
	
	

	@Override
	public Customer updateCustomer(Customer customer, Long id) {
		return customerRepository.save(customer);
	}

	@Override
	public Customer getCustomerById(Long id) {
		return customerRepository.findById(id).get();
	}

	@Override
	public void deleteCustomer(Long id) {
		customerRepository.deleteById(id);
	}

	@Override
	public Customer findByMobile(String mobile) {
		// TODO Auto-generated method stub
		return customerRepository.findByMobile(mobile).get();
	}

}
