package com.mwahba.cadeaux.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.mwahba.cadeaux.entitiy.Note;
import com.mwahba.cadeaux.repository.NoteRepository;
import com.mwahba.cadeaux.service.NoteService;

@Service
public class NoteServiceImpl implements NoteService {

	@Autowired
	NoteRepository noteRepository;

	@Override
	public List<Note> getAllNoted() {
		List<Note> notes= new ArrayList<>();
		Iterable<Note> findAll = noteRepository.findAll();
		findAll.forEach((note)->{
			notes.add(note);
		});
		return notes;
	}
	
	@Override
	public List<Note> getAllNotesPage(int page ,int size) {
		PageRequest pageRequest = PageRequest.of(page, size);
		List<Note> notes= new ArrayList<>();
		notes = noteRepository.findAll(pageRequest).toList();
		return notes;
	}

	@Override
	public Note createNote(Note newNote) {
		return noteRepository.save(newNote);
	}

	@Override
	public Note updateNote(Note newNote, Long id) {
		return noteRepository.save(newNote);
	}

	@Override
	public Note getNoteById(Long id) {
		return noteRepository.findById(id).get();
	}

	@Override
	public void deleteNote(Long id) {
		noteRepository.deleteById(id);
	}

}
