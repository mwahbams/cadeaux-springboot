package com.mwahba.cadeaux.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mwahba.cadeaux.entitiy.Customer;
import com.mwahba.cadeaux.entitiy.MyUser;
import com.mwahba.cadeaux.entitiy.MyUserDetails;
import com.mwahba.cadeaux.entitiy.Note;
import com.mwahba.cadeaux.repository.CustomerRepository;
import com.mwahba.cadeaux.repository.MyUserRepository;

@Service
public class CustomerDetailsService implements UserDetailsService {

	@Autowired
	MyUserRepository myUserRepository;
	
	@Autowired
	CustomerRepository customerRepository;

//	@Override
//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		Optional<MyUser> user = myUserRepository.findByUserName(username);
//		user.orElseThrow(()-> new UsernameNotFoundException("Not Found : " + username));
//		return user.map(MyUserDetails::new ).get();
//	}
	
	
//	@Override
//	public UserDetails loadUserByUsername(String mobile) throws UsernameNotFoundException {
//		Optional<MyUser> user = myUserRepository.findByMobile(mobile);
//		user.orElseThrow(()-> new UsernameNotFoundException("Not Found : " + mobile));
//		return user.map(MyUserDetails::new ).get();
//	}
	
	@Override
	public UserDetails loadUserByUsername(String mobile) throws UsernameNotFoundException {
		Optional<Customer> customer = customerRepository.findByMobile(mobile);
		customer.orElseThrow(()-> new UsernameNotFoundException("Not Found : " + mobile));
		return customer.map(MyUserDetails::new ).get();
	}
	
	public Customer findByMobile(String mobile) throws UsernameNotFoundException {
		Optional<Customer> customer = customerRepository.findByMobile(mobile);
		customer.orElseThrow(()-> new UsernameNotFoundException("Not Found : " + mobile));
		return customer.get();
	}

	public MyUser createUser(MyUser myUser) {
		myUser.setRoles("ROLE_ADMIN");
		myUser.setActive(true);
		return myUserRepository.save(myUser);
	}

}
