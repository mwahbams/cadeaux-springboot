package com.mwahba.cadeaux.service;

import java.util.List;

import com.mwahba.cadeaux.entitiy.PendingCommunication;
import com.mwahba.cadeaux.entitiy.UserVerificationRequest;
import com.mwahba.cadeaux.exception.BusinessException;

public interface PendingCommunicationService {
	
	public List<PendingCommunication> getAllPendingCommunication();
	public PendingCommunication createPendingCommunication(PendingCommunication pendingCommunication);
	public PendingCommunication signupPendingCommunication(PendingCommunication pendingCommunication);
	public PendingCommunication updatePendingCommunication(PendingCommunication pendingCommunication, Long id);
	public PendingCommunication getPendingCommunicationById(Long id);
	public void deletePendingCommunication(Long id);
	
	boolean checkVerification(String mobile, String verificationCode) throws BusinessException;
	
	void generateVerificationCode (UserVerificationRequest verificationRequest) throws BusinessException;
}
