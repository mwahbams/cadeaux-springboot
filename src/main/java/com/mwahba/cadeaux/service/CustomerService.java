package com.mwahba.cadeaux.service;

import java.util.List;
import java.util.Optional;

import com.mwahba.cadeaux.entitiy.Customer;

public interface CustomerService {
	
	public List<Customer> getAllCustomer();
	public Customer createCustomer(Customer customer);
	public Customer signupCustomer(Customer customer);
	public Customer updateCustomer(Customer customer, Long id);
	public Customer getCustomerById(Long id);
	public void deleteCustomer(Long id);
	
	Customer findByMobile(String mobile);
}
