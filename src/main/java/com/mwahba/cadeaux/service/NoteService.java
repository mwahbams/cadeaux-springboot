package com.mwahba.cadeaux.service;

import java.util.List;

import com.mwahba.cadeaux.entitiy.Note;

public interface NoteService {
	
	public List<Note> getAllNoted();
	
	public List<Note> getAllNotesPage(int page ,int size);
	public Note createNote(Note newNote);
	public Note updateNote(Note newNote, Long id);
	public Note getNoteById(Long id);
	public void deleteNote(Long id);
}
