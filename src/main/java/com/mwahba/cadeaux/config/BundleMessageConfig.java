package com.mwahba.cadeaux.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class BundleMessageConfig {
	
    @Bean(name = "messagesSource")
    public MessageSource messageSource() {
    	ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasenames("bundles.messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

}
